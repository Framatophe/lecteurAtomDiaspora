# Lecteur / afficheur de flux Atom pour Diapora*

Ce script PHP est largement inspiré de celui trouvé sur Scriptol (http://www.scriptol.fr/rss/lecteur-atom.php).

Sous licence MPL (Mozilla)

Il est adapté pour afficher un flux Diaspora, mais ce code est vraiment bricolé. Avis aux contributeurs qui voudraient le nettoyer !

# To do list

   * Simplifier le code
   * afficher la date et l'heure au format Europe/Paris

# Mode d'emploi

L'ensemble est constitué de deux fichiers : atomlib.php  et atom-affich.php.

atom-affich.php contient le script à placer là où l'on veut afficher le flux (c'est aussi à cet endroit que l'on spécifie l'adresse du flux).