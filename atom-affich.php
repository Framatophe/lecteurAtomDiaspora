<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" dir="ltr" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

	
<body bgcolor="#FFFFFF">
<h1>Demonstration</h1>

Charger un flux Atom et afficher son contenu  (<a href="http://www.scriptol.fr/rss/lecteur-atom.php">inspiré de ce script</a>)
<hr>
<br>
<p>
<?php
	require_once("atomlib.php");
	$url = "https://framasphere.org/public/framatophe.atom";  /* Mettre ici l'adresse du flux atom */
	echo Atom_Display($url, 10); /* ici le nombre de post que l'on souhaite afficher */
?>

</p>
</body>
</html>